module.exports = {
    "env": {
        "commonjs": true,
        "es6": true,
        "node": true,
        "jest":true
    },
    "extends": "eslint:recommended",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    // "parserOptions": {
    //     "ecmaVersion": 2018
    // },
    "rules": {
        // enable additional rules
    "indent": ["error", 4],
    "linebreak-style": ["error", "windows"],
    "quotes": ["error", "single"],
    "semi": ["error", "always"],
    // override default options for rules from base configurations
    //"comma-dangle": ["error", "always"],
    "no-cond-assign": ["error", "always"]
    }
};