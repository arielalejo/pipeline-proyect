const {divfunct} = require ('./indeX');

it('positive div function', ()=>{
    const returned_value = divfunct(10,5);
    const expected_value = 2.0;

    expect(returned_value).toBe(expected_value);
});

